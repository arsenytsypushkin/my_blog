---
title: New laptop
date: 2019-05-20
tags: ["laptop", "technology", "computer", "life"]
---

That's very hard nowadays to choose laptop for your needs.

Especially if you have lot's of hobbies and everyday activities with your computer.

I have been using pc since childhood and programming since late school and since then I started thinking, what should my laptop or pc looks like. 

When you have lots of free time, that's obvious that you will try to play computer games. So, at first you think, that game will rule on your machine.

Years go by and your priority changes. When I was in university, I was playing sometimes, but after I found first full-time job I quit playing regularly. 

And now, when I usually go to office every working, I still spend much time with computer at home. So I've decided, that despite the fact I have laptop from work, I need to have my own. To be more independant from work. 

Hopefully I've used a lot of OS types - Windows, Unix-based (debians mostly); laptop providers - Dell, Hp, Lenovo; all my machines were working based. Not for everyday life. And I've never used macOS.

That was a sign for me. Why not to try? All the specs was fine and the price didn't vary much from other laptops.

So, this blog I started with my new MacBook Pro 15 2018. For now, it works fine. Thanks, Steve, your company works well. (And earns well).

Maybe some time later I will describe more about my experience with this machine. But now that is all.

!["My mac"](/mac.jpg)
New stickers are warm welcome!