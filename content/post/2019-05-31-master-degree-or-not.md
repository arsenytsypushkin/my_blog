---
title: Master degree or not, that is the question
date: 2019-05-31
tags: ["studies", "life_blog"]
---

Hey there!

Today I'd like to talk about studies and degrees.

That is obvious, that school is a must for every citizen of every highly-performing country. Without it you will not be able to follow all the laws of the country, be polite and respectful to all the guys around you.

After it comes university or college. Every one who knows what does he\her want to do in his\her life should try to overcome yourself and come to the place like university. That will help you to know everything more fundamental and clear. It will structure thinking and solving different kinds of tasks starting from accommodation (different city\country) ending at theoretic or not bachelor thesis. Moreover you will join all the guys like you and this will lead you to smth sometimes disastrous and sometimes great.

Some of guys keep studying hard and dive into science or art and keep studying till PhD degree. That is great if it's bring you happiness and willing of life.

But at the same time, you should think about circumstances of your living. Flat, salary, business. And that lead you to work. Work much and hard to develop yourself and your life. Routine that keeps you healthy, wealthy and full.

And I am at this point now. I'm a big fan of science, but it's pretty hard to be a fulfillment or even part time employee and student that contribute much to some courses and studies.

That is my crossroad. What road will be the next?
