---
title: Welcome post
date: 2019-05-19
tags: ["hello_world", "easy_reading"]
bigimg: [{src: "/spb.jpg", desc: "Saint-Petersburg"}]
---

Hello, world! 

That is my first platform, where I can put thoughts from my head to .md files. It's not like [git](https://git-scm.com/), where you can just do everything as a developer. That is my platform about every aspect of life I want to share with anyone.

Some time before, I was thinking about sharing ideas in instagram, but the photo + idea format doesn't sutes me. I'm not a very passionate photographer, to tell you the truth.

But now, I've found a very easy-going [post](https://brainfood.xyz/post/20190518-host-your-own-blog-in-1-hour/), where you can easily set up your blog just in a short time. And that was a flashback from 2 years ago ideas.

So, please, don't judge me a lot 🙃. You are welcome to share your ideas with me via any of my social media above. Also, if you'll find mistakes, errors, bugs - feel free to share them with me. 

Thank you for reading not very helpful info for about 5-7 minutes.

Arseny.


