---
title: Who am I?
subtitle: Why should I spend my time here?
date: 2019-05-19
tags: ["me"]
comments: true
---

My name is Arseny Tsypushkin. 

I'm a software developer guy. All my development skills you can find on my linkedin account.

Speaking about my hobbies:

- Music - concerts, singing, listening a lot, just everyday for at least 2-3 hours.
- Theatre. Why you go to work, if you can just spend all days in the plays?
- Reading - now mostly development books, but also some fiction and non-fiction, scientific and prose.
- Sports - billiard, tennis (table and not), cycling, basketball.
- Travelling - if I have enough time and money.

Feel free to write me, if you have smth to discuss. Can't say about response time, but you can try.
